package com.nlmk.evteev;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class InvocationHandlerImpl implements InvocationHandler {

    private FactorialServiceImpl factorialService;
    private Map<Integer, Long> map = new HashMap<>();


    public InvocationHandlerImpl(FactorialServiceImpl factorialService) {
        this.factorialService = factorialService;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().equals("calculateFactorial")) {
            int val = (int) args[0];
            if (map.containsKey(val)) {
                return map.get(val);
            }
            Long result = (Long) method.invoke(factorialService, args);
            map.put(val, result);
            return result;
        }
        return method.invoke(factorialService, args);
    }

}
