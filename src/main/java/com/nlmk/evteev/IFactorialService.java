package com.nlmk.evteev;

public interface IFactorialService {

    Long calculateFactorial(int n);
}
