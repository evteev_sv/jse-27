package com.nlmk.evteev;

public class FactorialServiceImpl implements IFactorialService {

    public FactorialServiceImpl() {
    }

    /**
     * Процедура вычисления факториала целого числа
     *
     * @param n целое число
     * @return факториал этого числа
     */
    @Override
    public Long calculateFactorial(int n) {
        long result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * i;
        }
        return result;
    }
}
