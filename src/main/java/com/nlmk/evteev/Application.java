package com.nlmk.evteev;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Application {

    private static final Logger logger = java.util.logging.Logger.getLogger(Application.class.getName());
    private static final String CMD_EXIT = "exit";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        String cmd = "";
        FactorialServiceImpl factorialService = new FactorialServiceImpl();
        InvocationHandler handler = new InvocationHandlerImpl(factorialService);
        IFactorialService proxy = (IFactorialService) Proxy.newProxyInstance(factorialService.getClass().getClassLoader(),
                factorialService.getClass().getInterfaces(), handler);
        while (flag) {
            System.out.println("Введите число:");
            cmd = scanner.nextLine();
            if (CMD_EXIT.equals(cmd.toLowerCase())) {
                flag = false;
            }
            int val = 0;
            try {
                val = Integer.parseInt(cmd);
            } catch (NumberFormatException ne) {
                if (flag) {
                    logger.log(Level.SEVERE, "Неверный тип аргументов!");
                }
            }
            System.out.println(proxy.calculateFactorial(val));
        }
    }
}
